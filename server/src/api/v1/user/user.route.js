import { Router } from 'express';
import { validate } from 'express-validation';

import { authenticate } from '../../../middleware/auth';
import { updateUser } from '../../../validators/user.validation';
import * as controller from './user.controller';

const router = Router();

router.use(authenticate).use(controller.loadUser);

router
    .route('/')
    .get(controller.getUser)
    .patch(validate(updateUser), controller.updateUser)
    .delete(controller.deleteUser);

export default router;
