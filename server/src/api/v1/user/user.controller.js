import { NO_CONTENT } from 'http-status';
import { pick } from 'lodash';

import { deleteUserById } from '../../../services/user/deleteUser';
import { getUserById } from '../../../services/user/getUserById';
import { updateLocalUser } from '../../../services/user/updateUser';

export const loadUser = async (req, res, next) => {
    try {
        const user = await getUserById({ id: req.jwt?.sub });
        req.locals = { user };
        return next();
    } catch (error) {
        return next(error);
    }
};

export const getUser = (req, res, next) => {
    try {
        const filter = ['_id', 'name', 'email'];
        const user = req.locals.user;

        return res.send(pick(user, filter));
    } catch (err) {
        return next(err);
    }
};

export const updateUser = async (req, res, next) => {
    try {
        const { email, name, password } = req.body || {};
        const currentUserId = req.locals.user._id;

        const user = await updateLocalUser(currentUserId, { email, name, password });
        req.locals = { user };

        return getUser(req, res, next);
    } catch (err) {
        return next(err);
    }
};

// controller implementare delete
export const deleteUser = async (req, res, next) => {
    try {
        const id = req.locals.user._id;
        await deleteUserById({ id });
        res.status(NO_CONTENT).send();
    } catch (err) {
        return next(err);
    }
};
