import { Router } from 'express';

import authRoutesV1 from './auth/auth.route';
import companyRoutesV1 from './company/company.route';
import userRoutesV1 from './user/user.route';

const router = Router();

router.get('/status', (req, res) => res.send('OK'));

router.use('/user', userRoutesV1);
router.use('/auth', authRoutesV1);
router.use('/company', companyRoutesV1);

export default router;
