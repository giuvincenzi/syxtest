import { Router } from 'express';
import { validate } from 'express-validation';

import { login } from '../../../validators/auth.validation';
import { createUser } from '../../../validators/user.validation';
import { login as loginCtrl, register } from './auth.controller';

const router = Router();

router.route('/register').post(validate(createUser), register);
router.route('/login').post(validate(login), loginCtrl);

export default router;
