import { CREATED } from 'http-status';
import { pick } from 'lodash';

import { loginWithUsrPwd } from '../../../services/auth/login';
import { generateToken } from '../../../services/auth/token';
import { createUser } from '../../../services/user/createUser';

export const register = async (req, res, next) => {
    try {
        const { email, name, password } = req.body;

        const user = await createUser({ email, name, password });
        const token = generateToken({ userId: user._id.toString() });

        const filter = ['_id', 'name', 'email'];
        const resBody = {
            user: pick(user.toJSON(), filter),
            token,
        };
        res.status(CREATED).send(resBody);
    } catch (err) {
        return next(err);
    }
};

export const login = async (req, res, next) => {
    try {
        const { email, password } = req.body;
        const user = await loginWithUsrPwd({ email, password });
        const token = generateToken({ userId: user._id.toString() });

        const filter = ['_id', 'name', 'email'];
        const resBody = {
            user: pick(user.toJSON(), filter),
            token,
        };

        res.send(resBody);
    } catch (err) {
        return next(err);
    }
};
