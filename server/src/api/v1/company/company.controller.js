import { CREATED, NO_CONTENT } from 'http-status';

import { createCompany as createCompanyService } from '../../../services/company/createCompany';
import { deleteCompanyById } from '../../../services/company/deleteCompany';
import { getAllByUserId } from '../../../services/company/getAllByUserId';
import { getCompanyById } from '../../../services/company/getCompanyById';
import { updateLocalCompany } from '../../../services/company/updateCompany';

export const createCompany = async (req, res, next) => {
    try {
        const { companyName, vat, description } = req.body;

        const company = await createCompanyService({ companyName, vat, description, userId: req.jwt.sub });

        res.status(CREATED).send(company);
    } catch (err) {
        return next(err);
    }
};

export const loadCompany = async (req, res, next, id) => {
    try {
        const company = await getCompanyById({ id });
        req.locals = { company };
        return next();
    } catch (error) {
        return next(error);
    }
};

export const getCompany = (req, res, next) => {
    try {
        const company = req.locals.company;

        return res.send(company);
    } catch (err) {
        return next(err);
    }
};

export const getAllCompanyByUser = async (req, res, next) => {
    try {
        const data = await getAllByUserId({ id: req.jwt.sub });

        return res.send(data);
    } catch (err) {
        return next(err);
    }
};

export const updateCompany = async (req, res, next) => {
    try {
        const { companyName, vat, description, reviewRating, reviewCount } = req.body || {};
        const currentCompanyId = req.locals.company._id;

        const company = await updateLocalCompany(currentCompanyId, {
            companyName,
            vat,
            description,
            reviewRating,
            reviewCount,
        });
        req.locals = { company };

        return getCompany(req, res, next);
    } catch (err) {
        return next(err);
    }
};

// controller implementare delete
export const deleteCompany = async (req, res, next) => {
    try {
        const id = req.locals.company._id;
        await deleteCompanyById({ id });
        res.status(NO_CONTENT).send();
    } catch (err) {
        return next(err);
    }
};
