import { Router } from 'express';
import { validate } from 'express-validation';

import { authenticate } from '../../../middleware/auth';
import { companyId, createCompany, updateCompany } from '../../../validators/company.validation';
import * as controller from './company.controller';

const router = Router();

router.route('/create').post(authenticate, validate(createCompany), controller.createCompany);
router.route('/allByUser').get(authenticate, controller.getAllCompanyByUser);

router.param('companyId', authenticate);
router.param('companyId', validate(companyId));
router.param('companyId', controller.loadCompany);

router
    .route('/:companyId')
    .get(controller.getCompany)
    .patch(validate(updateCompany), controller.updateCompany)
    .delete(controller.deleteCompany);

export default router;
