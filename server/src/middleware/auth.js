import { FORBIDDEN, UNAUTHORIZED } from 'http-status';
import jwt from 'jsonwebtoken';

import config from '../config';
import { ApiError } from '../utils/ApiError';

const { JWT_SECRET } = config;

export const authenticate = (req, res, next) => {
    const token = req.headers['authorization']?.split(' ')[1];

    if (!token) {
        return next(new ApiError({ status: FORBIDDEN, message: 'Forbidden' }));
    }

    try {
        const decoded = jwt.verify(token, JWT_SECRET);
        req.jwt = decoded;
    } catch (err) {
        return next(new ApiError({ status: UNAUTHORIZED, message: 'Invalid Token' }));
    }
    return next();
};
