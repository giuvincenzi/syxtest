import { ValidationError } from 'express-validation';
import httpStatus from 'http-status';

import config from '../config';
import { ApiError } from '../utils/ApiError';

export const errorHandler = (err, req, res, next) => {
    const { NODE_ENV } = config;

    const response = {
        code: err.status || httpStatus.INTERNAL_SERVER_ERROR,
        message: err.message || httpStatus[err.status],
        details: err.details,
        stack: err.stack,
    };

    if (NODE_ENV !== 'development') {
        delete response.stack;
    }

    console.error(err);
    res.status(response.code).send(response);
};

export const errorConverter = (err, req, res, next) => {
    let convertedError = err;

    if (err instanceof ValidationError) {
        convertedError = new ApiError({
            message: err.message,
            details: Object.keys(err.details)
                .map(k => err.details[k].map(({ message }) => message))
                .flat(),
            status: err.statusCode,
            stack: err.stack,
        });
    } else if (!(err instanceof ApiError)) {
        convertedError = new ApiError({
            message: err.message,
            status: err.status,
            stack: err.stack,
        });
    }

    return errorHandler(convertedError, req, res);
};

export const notFound = (req, res, next) => {
    const err = new ApiError({
        message: 'Not found',
        status: httpStatus.NOT_FOUND,
    });
    return errorHandler(err, req, res);
};
