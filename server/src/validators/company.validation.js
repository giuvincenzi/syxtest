import { Joi } from 'express-validation';

export const companyId = {
    params: Joi.object({
        companyId: Joi.string()
            .regex(/^[a-fA-F0-9]{24}$/)
            .required(),
    }),
};

export const createCompany = {
    body: Joi.object({
        companyName: Joi.string().min(2).max(50).required(),
        vat: Joi.string().min(2).max(50).required(),
        description: Joi.string().max(128),
    }),
};

export const updateCompany = {
    body: Joi.object({
        companyName: Joi.string().min(2).max(50),
        vat: Joi.string().min(2).max(50),
        description: Joi.string().max(128),
        reviewRating: Joi.number(),
        reviewCount: Joi.number(),
    }),
    params: Joi.object({
        companyId: Joi.string()
            .regex(/^[a-fA-F0-9]{24}$/)
            .required(),
    }),
};
