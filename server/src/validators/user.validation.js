import { Joi } from 'express-validation';

export const userId = {
    params: Joi.object({
        userId: Joi.string()
            .regex(/^[a-fA-F0-9]{24}$/)
            .required(),
    }),
};

export const createUser = {
    body: Joi.object({
        email: Joi.string().email().required(),
        password: Joi.string().min(6).max(128).required(),
        name: Joi.string().max(128),
    }),
};

export const updateUser = {
    body: Joi.object({
        email: Joi.string().email(),
        password: Joi.string().min(6).max(128),
        name: Joi.string().max(128),
    }),
    params: Joi.object({
        userId: Joi.string()
            .regex(/^[a-fA-F0-9]{24}$/)
            .required(),
    }),
};
