import { getUserById } from './getUserById';

export const deleteUserById = async ({ id }) => {
    const user = await getUserById({ id });
    await user.remove();

    return null;
};
