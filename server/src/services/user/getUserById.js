import { NOT_FOUND } from 'http-status';

import { ApiError } from '../../utils/ApiError';
import { userModel } from './../../models/userModel';

export const getUserById = async ({ id }) => {
    const user = await userModel.findById(id);
    if (!user) {
        throw new ApiError({ status: NOT_FOUND, message: `User with id "${id}" not found!` });
    }
    return user;
};
