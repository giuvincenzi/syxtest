import { userModel } from './../../models/userModel';
import { throwErrorIfEmailExist } from './createUser';
import { getUserById } from './getUserById';

export const updateLocalUser = async (id, { name, email, password } = {}) => {
    if (email) await throwErrorIfEmailExist(email);
    const newUserProps = { name, email, password };

    await userModel.updateOne({ id }, newUserProps);
    const user = await getUserById({ id });

    return user;
};
