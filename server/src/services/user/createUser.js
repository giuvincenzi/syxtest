import { CONFLICT } from 'http-status';

import { ApiError } from '../../utils/ApiError';
import { userModel } from './../../models/userModel';

export const createUser = async ({ name, email, password }) => {
    await throwErrorIfEmailExist(email);
    const user = await userModel.create({
        name,
        email,
        password,
    });
    return user;
};

export async function throwErrorIfEmailExist(email) {
    if (await userModel.emailExist(email)) {
        throw new ApiError({ status: CONFLICT, message: 'Email already exist' });
    }
}
