import { connect } from 'mongoose';

export const dbConnect = async ({ host, port }) => {
    console.info('Database connecting...');
    const db = await connect(`${host}:${port}`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });

    console.info(`connected to "${db.connections[0].host}" "${db.connections[0].name}"`);
    return db;
};
