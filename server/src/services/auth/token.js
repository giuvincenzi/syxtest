import jwt from 'jsonwebtoken';
import moment from 'moment-timezone';

import config from '../../config';

const { JWT_SECRET, JWT_EXPIRATION_MINUTES } = config;

export const generateToken = ({ userId }) => {
    const payload = {
        exp: moment().add(JWT_EXPIRATION_MINUTES, 'minutes').unix(),
        iat: moment().unix(),
        sub: userId,
    };

    const token = jwt.sign(payload, JWT_SECRET);
    return token;
};
