import bcrypt from 'bcryptjs';
import { UNAUTHORIZED } from 'http-status';

import { ApiError } from '../../utils/ApiError';
import { userModel } from './../../models/userModel';

export const loginWithUsrPwd = async ({ email, password }) => {
    const user = await userModel.findOne({ email });

    if (!user || !(await isValidPassword(password, user.password))) {
        throw new ApiError({ status: UNAUTHORIZED, message: 'Incorrect email or password' });
    }
    return user;
};

async function isValidPassword(pwd, pwdInput) {
    const isValid = await bcrypt.compare(pwd, pwdInput);
    return isValid;
}
