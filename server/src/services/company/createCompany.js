import { CONFLICT } from 'http-status';

import { ApiError } from '../../utils/ApiError';
import { companyModel } from './../../models/companyModel';

export const createCompany = async ({ companyName, vat, description, userId } = {}) => {
    await throwErrorIfVatExist(vat);
    const company = await companyModel.create({
        companyName,
        vat,
        description,
        userId,
    });
    return company;
};

export async function throwErrorIfVatExist(vat) {
    if (await companyModel.vatExist(vat)) {
        throw new ApiError({ status: CONFLICT, message: 'Vat already exist' });
    }
}
