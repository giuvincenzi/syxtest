import { NOT_FOUND } from 'http-status';

import { ApiError } from '../../utils/ApiError';
import { companyModel } from './../../models/companyModel';

export const getCompanyById = async ({ id }) => {
    const company = await companyModel.findById(id);
    if (!company) {
        throw new ApiError({ status: NOT_FOUND, message: `Company with id "${id}" not found!` });
    }
    return company;
};
