import { companyModel } from './../../models/companyModel';

export const getAllByUserId = async ({ id }) => {
    const company = await companyModel.find({ userId: id });

    return company;
};
