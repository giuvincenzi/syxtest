import { companyModel } from './../../models/companyModel';
import { throwErrorIfVatExist } from './createCompany';
import { getCompanyById } from './getCompanyById';

export const updateLocalCompany = async (id, { companyName, vat, description, reviewRating, reviewCount } = {}) => {
    if (vat) await throwErrorIfVatExist(vat);
    const newCompanyProps = { companyName, vat, description, reviewRating, reviewCount };

    await companyModel.updateOne({ id }, newCompanyProps);
    const company = await getCompanyById({ id });

    return company;
};
