import { getCompanyById } from './getCompanyById';

export const deleteCompanyById = async ({ id }) => {
    const user = await getCompanyById({ id });
    await user.remove();

    return null;
};
