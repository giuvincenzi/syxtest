export class ApiError extends Error {
    constructor({ status, message, stack, isOperational, details }) {
        super(message);
        this.status = status;
        this.isOperational = isOperational ?? true;
        this.details = details;

        if (stack) {
            this.stack = stack;
        } else {
            Error.captureStackTrace(this, this.constructor);
        }
    }
}
