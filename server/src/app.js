import compression from 'compression';
import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import methodOverride from 'method-override';
import xss from 'xss-clean';

import apiRouteV1 from './api/v1';
import { errorConverter, errorHandler, notFound } from './middleware/errors';

export default () => {
    const app = express();

    // set secure headers
    app.use(helmet());

    // parsers
    app.use(express.json({ limit: '25mb' }));
    app.use(express.urlencoded({ extended: true }));

    // sanitizes
    app.use(xss());

    // gzip compression
    app.use(compression());

    // for use HTTP verbs such as PUT or DELETE
    app.use(methodOverride());

    // cors
    app.use(cors());
    app.options('*', cors());

    // mount api v1 routes
    app.use('/api/v1', apiRouteV1);

    // if error is not an instanceOf APIError, convert it.
    app.use(errorConverter);

    // 404 fallback route
    app.use(notFound);

    //error hander
    app.use(errorHandler);

    return app;
};
