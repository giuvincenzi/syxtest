import dotenv from 'dotenv';
import joi from 'joi';
import path from 'path';

const defaultConfig = {
    API_PORT: '3110',
    JWT_SECRET: 'thesecret',
    JWT_EXPIRATION_MINUTES: '120',
};
const config = {
    ...defaultConfig,
    ...(dotenv.config({
        path: path.join(__dirname, '../.env'),
    }).parsed ?? {}),
};

const schema = joi
    .object({
        NODE_ENV: joi.string().valid('production', 'development').required(),
        MONGO_HOST: joi.string().required(),
        MONGO_PORT: joi.string().required(),
        API_PORT: joi.string(),
        JWT_SECRET: joi.string(),
        JWT_EXPIRATION_MINUTES: joi.string(),
    })
    .unknown(false);

const validation = schema.validate(config);

if (validation.error) throw new Error(validation.error);

export default config;
