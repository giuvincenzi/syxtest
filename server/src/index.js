import http from 'http';

import app from './app';
import config from './config';
import { dbConnect } from './services/database';

(async () => {
    const { MONGO_HOST, MONGO_PORT, API_PORT } = config;
    // db connection
    await dbConnect({
        host: MONGO_HOST,
        port: MONGO_PORT,
    });

    // create server
    const server = http.createServer(app());

    // run server
    await server.listen(API_PORT);
    console.info(`Server listening on port ${API_PORT}`);
})();

process
    .on('SIGTERM', function todo() {})
    .on('SIGINT', function todo() {})
    .on('uncaughtException', function todo() {});
