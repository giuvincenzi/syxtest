import { model, Schema } from 'mongoose';

const reqTrim = {
    required: true,
    trim: true,
};
const companySchema = new Schema(
    {
        companyName: { ...reqTrim, type: String },
        vat: { ...reqTrim, type: String },
        description: { ...reqTrim, type: String },
        reviewRating: {
            type: Number,
            default: 0,
        },
        reviewCount: {
            type: Number,
            default: 0,
        },
        userId: {
            type: Schema.Types.ObjectId,
            required: true,
        },
    },
    { timestamps: true },
);

companySchema.statics.vatExist = async function (vat) {
    const model = this;
    const companyVat = await model.findOne({ vat });
    return !!companyVat;
};

export const companyModel = model('Company', companySchema);
