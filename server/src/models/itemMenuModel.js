import { model, Schema } from 'mongoose';

const reqTrim = {
    required: true,
    trim: true,
};
const itemMenuSchema = new Schema(
    {
        description: { ...reqTrim, type: String },
        price: { ...reqTrim, type: Number },
        menuId: {
            type: Schema.Types.ObjectId,
            required: true,
        },
    },
    { timestamps: true },
);

export const itemMenuModel = model('ItemMenu', itemMenuSchema);
