import { model, Schema } from 'mongoose';

const reqTrim = {
    required: true,
    trim: true,
};
const menuItemSchema = new Schema(
    {
        type: { ...reqTrim, type: String },
        description: { ...reqTrim, type: String },
        companyId: {
            type: Schema.Types.ObjectId,
            required: true,
        },
        isDefault: {
            type: Boolean,
            default: false,
        },
    },
    { timestamps: true },
);

export const menuItemModel = model('ItemMenu', menuItemSchema);
