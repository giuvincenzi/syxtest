import bcrypt from 'bcryptjs';
import { model, Schema } from 'mongoose';

const reqTrim = {
    required: true,
    trim: true,
};
const userSchema = new Schema(
    {
        name: { ...reqTrim, type: String },
        email: {
            ...reqTrim,
            type: String,
            unique: true,
            lowercase: true,
            match: /^\S+@\S+\.\S+$/,
        },
        password: {
            ...reqTrim,
            type: String,
            minlength: 6,
            maxlength: 128,
        },
    },
    { timestamps: true },
);

userSchema.pre('save', async function (next) {
    const user = this;
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8);
    }
    next();
});
userSchema.pre('updateOne', async function (next) {
    const user = this;
    if (user._update.password) {
        user._update.password = await bcrypt.hash(user._update.password, 8);
    }
    next();
});

userSchema.statics.emailExist = async function (email) {
    const model = this;
    const userEmail = await model.findOne({ email });
    return !!userEmail;
};

export const userModel = model('User', userSchema);
