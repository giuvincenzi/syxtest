import { model, Schema } from 'mongoose';

const itemMenuSchema = new Schema(
    {
        ref: {
            type: Schema.Types.ObjectId,
            required: true,
        },
        filename: { type: String, required: true },
        mimetype: { type: String, required: true },
        encoding: { type: String, required: true },
        uri: { type: String, required: true },
    },
    { timestamps: true },
);

export const itemMenuModel = model('ItemMenu', itemMenuSchema);
