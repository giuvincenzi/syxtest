## Development

Make sure you are in the root of the Server project and run:

```bash
cp envs/syxtest.env .env
npm run dev
```

`syxtest.env` file contains the environment variables of the service.

The server needs the .env file in the root of the server project. In case you want to change the database host or else open and modify the `.env` file.

[Postman collection](./syxtest.postman_collection.json)

## To do

-   add graphql
-   add tests
-   add logger
-   implement and manage roles, currently apis do not have logic based on roles
-   add husky to handle git flow
-   dockerize service
-   add production sourcemap
-   add CD/CI pipeline

