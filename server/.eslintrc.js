module.exports = {
    extends: ['@lib/eslint-config-global'],
    root: true,
    rules: {
        'no-unused-vars': ['warn', { argsIgnorePattern: 'next' }],
    },
};
