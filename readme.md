# Getting started

### Engines
- node: `^16.0.0`
- npm: `^8.0.0`

## Install

```bash
npm ci
```

For run server read [Server README](/server)

