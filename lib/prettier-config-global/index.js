module.exports = {
    printWidth: 120,
    tabWidth: 4,
    semi: true,
    singleQuote: true,
    arrowParens: "avoid",
    trailingComma: "all",
    overrides: [
        {
            files: ["*.yml", "*.yaml", "*.json"],
            options: {
                tabWidth: 2,
            },
        },
    ],
};
