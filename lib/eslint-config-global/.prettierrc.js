module.exports = {
    printWidth: 120,
    tabWidth: 4,
    trailingComma: 'none',
    semi: true,
    singleQuote: true,
    arrowParens: 'avoid'
};
