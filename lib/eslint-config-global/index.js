module.exports = {
    parser: '@babel/eslint-parser',
    parserOptions: {
        requireConfigFile: false
    },
    plugins: ['simple-import-sort'],
    extends: ['eslint:recommended', 'plugin:import/recommended', 'plugin:prettier/recommended'],
    env: {
        node: true,
        commonjs: true,
        es2021: true
    },
    ignorePatterns: ['.eslintrc.js'],
    rules: {
        'no-console': ['warn', { allow: ['warn', 'error', 'info'] }],
        'no-unused-vars': 'warn',
        'no-return-await': 'warn',
        'require-await': 'warn',
        'no-useless-return': 'warn',
        eqeqeq: 'warn',
        'import/no-cycle': 'warn',
        'import/prefer-default-export': 'off',
        'import/no-named-as-default': 'off',
        'prettier/prettier': 'warn',
        'sort-imports': 'off',
        'simple-import-sort/imports': 'warn',
        'simple-import-sort/exports': 'warn'
    }
};
